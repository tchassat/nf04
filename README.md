# NF04

Mini projet de l'uv NF04 de l'UTC. Modélisation numérique des problèmes de l'ingénieur. 

Le mini projet porte sur la résolution d'un problème de thermique. Nous  étudions le comportement thermique d'une ailette de refroidissement de carte graphique, à l'aide de la méthode des éléments finis. 

Ce répertoire comprend : 
- le rapport au format tex et au format pdf
- l'ensemble des fichiers nécessaires à la résolution du problème stationnaire
- l'ensemble des fichiers nécessaires à la résolution du problème non stationnaire
